## Input data
In this solution a cropped region from the dataset linked in the citations is used:
![The input data](./data/input_mask.tif)

## Results
The resulting plots were generated running this solution.

![output plot 1](./data/output/directions_0:5496:0:5352.png)
![output plot 2](./data/output/directions_0:5496:0:5352_tile100.png)
![output plot 3](./data/output/directions_0:5496:0:5352_tile200.png)
![output plot 4](./data/output/directions_0:5496:0:5352_tile300.png)
![output plot 5](./data/output/directions_0:5496:0:5352_tile400.png)
![output plot 6](./data/output/directions_0:5496:0:5352_tile500.png)
![output plot 6](./data/output/ROIs.png)
![output plot 6](./data/output/directions_100:3000:1000:3000.png)
![output plot 6](./data/output/directions_100:3000:1000:3000_tile100.png)
![output plot 6](./data/output/directions_100:3000:1000:3000_tile200.png)
![output plot 6](./data/output/directions_100:3000:1000:3000_tile300.png)
![output plot 6](./data/output/directions_100:3000:1000:3000_tile400.png)
![output plot 6](./data/output/directions_100:3000:1000:3000_tile500.png)
![output plot 6](./data/output/directions_2000:1000:3500:2000.png)
![output plot 6](./data/output/directions_2000:1000:3500:2000_tile100.png)
![output plot 6](./data/output/directions_2000:1000:3500:2000_tile200.png)
![output plot 6](./data/output/directions_2000:1000:3500:2000_tile300.png)
![output plot 6](./data/output/directions_2000:1000:3500:2000_tile400.png)
![output plot 6](./data/output/directions_2000:1000:3500:2000_tile500.png)
