from io import StringIO

from album.runner.api import setup

env_file = StringIO("""name:  macrophage-brain-vasculature
channels:
  - conda-forge
  - defaults
dependencies:
  - pyyaml
  - python==3.8
  - pip
  - numpy
  - Pillow
  - pyimagej
  - opencv
  - matplotlib
  - scipy
  - pandas
  - seaborn
  - tifffile
  - scikit-learn
  - openjdk >=11.0.6
  - git
  - pip:
      - requests
      - pyinstaller
""")


setup(
    group="de.mdc-berlin",
    name="mbv_parent",
    version="0.1.0-SNAPSHOT",
    title="macrophage-brain-vasculature mbv_parent solution",
    description="A set of solutions for localizing macrophages, segmenting brain vasculature, and tumors",
    solution_creators=["Kyle Harrington", "Jan Philipp Albrecht"],
    tags=["vasculature", "segmentation", "projection", "hidden"],
    license="Apache v2",
    documentation=["README.md"],
    album_api_version="0.5.1",
    args=[],
    dependencies={
        'environment_file': env_file
    }
)
