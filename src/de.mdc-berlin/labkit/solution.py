import os.path

from album.runner.api import setup


def install():
    import subprocess
    import shutil
    from album.runner.api import get_app_path, get_package_path

    get_app_path().mkdir(exist_ok=True, parents=True)

    # copy source files into solution app folder
    shutil.copy(get_package_path().joinpath('build.gradle'), get_app_path())
    shutil.copytree(get_package_path().joinpath('src'), get_app_path().joinpath('src'))

    # compile app
    subprocess.run([shutil.which('gradle'), 'build', '-Dorg.gradle.internal.http.socketTimeout=300000'], cwd=get_app_path())


def run():
    import shutil
    import subprocess
    from album.runner.api import get_app_path, get_args
    params = "\"%s\"" % os.path.abspath(get_args().input)
    # run app via maven
    subprocess.run([shutil.which('gradle'), 'run', '-q', '--args=%s' % params], cwd=get_app_path())


setup(
    group="de.mdc-berlin",
    name="labkit",
    version="0.1.0-SNAPSHOT",
    authors=["Deborah Schmidt"],
    title="Labkit: Labeling and Segmentation Toolkit for Big Image Data",
    description="This tool can be used to annotate data and create segmentations using the built-in random forest based pixel classifier.",
    cite=[{
        "text": "Arzt, Matthias & Deschamps, Joran & Schmied, Christopher & Pietzsch, Tobias & Schmidt, Deborah & Tomancak, Pavel & Haase, Robert & Jug, Florian. (2022). LABKIT: Labeling and Segmentation Toolkit for Big Image Data. Frontiers in Computer Science. 4.",
        "doi": "10.3389/fcomp.2022.777728",
        "url": "https://imagej.net/plugins/labkit/"
    }],
    tags=["labkit", "random forest", "segmentation", "annotation"],
    album_api_version="0.4.2",
    args=[{
            "name": "input",
            "type": "file",
            "required": True,
            "description": "The raw data file to load in Labkit (common image data types or a BDV .xml file)"
        }],
    covers=[{
        "description": "Screenshot of Labkit showing the resulting segmentation of a trained classifier.",
        "source": "cover.png"
    }],
    install=install,
    run=run,
    dependencies={'parent': {'resolve_solution': 'album:template-gradle-java11:0.1.0'}}
)

