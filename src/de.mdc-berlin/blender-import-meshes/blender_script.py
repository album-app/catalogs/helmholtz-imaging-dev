import logging
import os
import sys
from pathlib import Path

import bpy

# logging.info(str(sys.argv))
decimate_ratio = float(sys.argv[len(sys.argv)-5])
resolution_percentage = int(sys.argv[len(sys.argv)-4])
in_path = sys.argv[len(sys.argv)-3].strip()
out_blend_path = sys.argv[len(sys.argv)-2]
if out_blend_path == "None":
    out_blend_path = None
out_rendering_path = sys.argv[len(sys.argv)-1]
if out_rendering_path == "None":
    out_rendering_path = None
scene = bpy.context.scene

main_collection = bpy.data.collections['Collection']
objs = set()

# for obj in main_collection.objects:
#     objs.add( obj.data )
#     bpy.data.objects.remove( obj )
cube = bpy.data.objects['Cube']
bpy.data.objects.remove( cube )


def adjust_newly_added_object():
    global obj
    bpy.ops.object.shade_smooth()
    obj = bpy.context.object
    if decimate_ratio < 1.0:
        bpy.ops.object.editmode_toggle()
        bpy.ops.mesh.decimate(ratio=decimate_ratio)
        bpy.ops.object.editmode_toggle()


if in_path.endswith('.stl'):
    bpy.ops.import_mesh.stl(filepath=str(Path(in_path).absolute()))
    adjust_newly_added_object()
elif in_path.endswith('.obj'):
    bpy.ops.import_mesh.obj(filepath=str(Path(in_path).absolute()))
    adjust_newly_added_object()
else:
    for subdir, dirs, files in os.walk(in_path):

        file_list = [item for item in files if item.endswith('.stl')]
        logging.info("files : ")
        logging.info(file_list)
        file_list.sort()
        for file in file_list:
            # logging.info(file)
            name = os.path.join(subdir, file)
            bpy.ops.import_mesh.stl(filepath = name)
            adjust_newly_added_object()

        file_list = [item for item in files if item.endswith('.obj')]
        file_list.sort()
        for file in file_list:
            # logging.info(file)
            name = os.path.join(subdir, file)
            bpy.ops.import_scene.obj(filepath = name)
            adjust_newly_added_object()

# focus on data collection
objects = bpy.context.scene.objects
for obj in objects:
    obj.select_set(obj.type == "MESH")
bpy.ops.view3d.camera_to_view_selected()

light = bpy.data.objects['Light']
light.data.type = 'SUN'
light.data.energy = 10

# increase clip end to make sure also bigger datasets are visible
cam_ob = bpy.context.scene.camera
cam_ob.data.clip_end = 30000
cam_ob.data.lens = 45

# show camera view
area = next(area for area in bpy.context.screen.areas if area.type == 'VIEW_3D')
area.spaces[0].region_3d.view_perspective = 'CAMERA'

if out_blend_path:
    bpy.ops.wm.save_as_mainfile(filepath=out_blend_path)
# bpy.context.window.workspace = bpy.data.workspaces["Rendering"]

# set background color
world = bpy.data.worlds['World']
world.use_nodes = True
bg = world.node_tree.nodes['Background']
bg.inputs[0].default_value[:3] = (0.01,0.01,0.01)

# render
# bpy.context.scene.render.resolution_x = w
# bpy.context.scene.render.resolution_y = h
bpy.context.scene.render.resolution_percentage = resolution_percentage
bpy.context.scene.render.engine = 'CYCLES'
if out_rendering_path:
    bpy.context.scene.render.filepath = out_rendering_path
    bpy.ops.render.render('INVOKE_DEFAULT', write_still=True)


bpy.ops.wm.save_as_mainfile(filepath=out_blend_path)
