---
layout: page
title: About
permalink: /about/
---

This catalog is a collection of [Album](https://album.solutions) solutions  in development before they are potentially deployed to [a curated collection of Helmholtz Imaging Album Solutions](https://helmholtz-imaging.pages.hzdr.de/album-solutions).  
These solutions are developed for past or ongoing collaborations, projects, or support requests at Helmholtz Imaging and wrap existing tools or algorithms for common image data analysis problems.
Please refer to the citation displayed for each solution in order to cite the authors of the tools used in each solution. 

Since the solutions in this catalog are deployed in a development state, they might not work or behave unexpectedly. Use them at your own risk.