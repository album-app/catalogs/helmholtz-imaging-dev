module.exports = {
  pathPrefix: `/catalogs/helmholtz-imaging-dev`,
  siteMetadata: {
    title: 'Image data analysis solutions - development catalog',
    subtitle: 'Unstable list of solutions in development, curated by Helmholtz Imaging',
    catalog_url: 'https://gitlab.com/album-app/catalogs/helmholtz-imaging-dev',
    menuLinks:[
      {
         name:'Catalog',
         link:'/catalog'
      },
      {
         name:'About',
         link:'/about'
      },
    ]
  },
  plugins: [{ resolve: `gatsby-theme-album`, options: {} }],
}
