from album.runner.api import setup

env_file = """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.10
  - pyvista=0.37.0
  - netCDF4=1.6.2
  - vtk=9.2.2
  - scipy=1.9.3
  - numpy=1.24.0
  - scikit-image=0.19.3
  - opencv=4.6.0
  - pandas=1.5.3
  - imageio=2.23.0
  - imageio-ffmpeg=0.4.7
  - matplotlib-base=3.5.3
  - ffmpeg=4.4.2
  - ffmpeg-python=0.2.0
  - geopandas=0.12.2
  - geopandas-base=0.12.2
  - matplotlib-base=3.5.3
  - regionmask=0.9.0
  - pip
  - pip:
    - https://gitlab.com/ida-mdc/earth-watch/-/archive/0.1.0/earth-watch-0.1.0.zip

"""

def run():

    from album.runner.api import get_args
    from earth_watch import renderer

    args = get_args()

    path_input_mesh = args.input_mesh
    path_input_color = args.input_color
    path_output = args.output
    path_input_events = args.input_events if str(args.input_events) is not 'None' else None

    n_timepoints = int(args.n_timepoints) if str(args.n_timepoints) is not 'None' else None
    lat_range = [float(it) for it in (args.lat_range).split(" ")
                ] if str(args.lat_range) is not 'None' else None
    long_range = [float(it) for it in (args.long_range).split(" ")
                ] if str(args.long_range) is not 'None' else None
    is_interp = args.is_interp if str(args.is_interp) is not 'None' else None
    cube_size = args.cube_size
    region_name = args.region_name
    bg_padding = args.bg_padding
    cube_height_multiplier = float(args.cube_height_multiplier)

    title = args.title if str(args.title) is not 'None' else None
    clim = [float(it) for it in (args.clim).split(" ")]
    cmap = str(args.cmap)

    frame_rate = args.frame_rate if str(args.n_timepoints) is not 'None' else 30
    logo_path = args.logo_path if str(args.logo_path) is not 'None' else None

    renderer.run_render(path_input_mesh, path_input_color, clim, path_output, 
                n_timepoints, region_name, lat_range, long_range, is_interp,
                cube_size, cmap, path_input_events, title, bg_padding, cube_height_multiplier,
                frame_rate, logo_path)

setup(
    group="de.ufz",
    name="earth-watch",
    version="0.1.0",
    title="Earth Watch",
    description="create a video of rendered (pyvista) sequential time points of netCDF data.",
    cite=[],
    tags=["netCDF", "pyvista", "render", "python", "visualisation"],
    license="MIT",
    #documentation=["documentation.md"], ### MISSING!!
    covers=[{
        "description": "render timepoint example",
        "source": "cover.png"
    }],
    album_api_version="0.5.1",
    args=[{
        "name": "input_mesh",
        "type": "file", 
        "required": True,
        "description": "path to netCDF dataset to base the mesh shape on"
        },
        {
        "name": "input_color",
        "type": "file", 
        "required": True,
        "description": "path to netCDF dataset to base the mesh color on"
        },
        {
        "name": "clim",
        "type": "string", 
        "required": True,
        "description": "color legend clim (min max) - format e.g. `0 100.`"
        },
        {
        "name": "output",
        "type": "file",
        "default": "earth_watch.mp4",
        "description": "path to the output movie - should end with .mp4"
        },
        {
        "name": "n_timepoints",
        "type": "integer",
        "default": 20,
        "description": "number of timepoints to be rendered (starting from first time point)"
        },
        {
        "name": "region_name",
        "type": "string",
        "default": None,
        "description": "Name of country or continent (capitalize first letter) to use as a region mask"
        },
        {
        "name": "lat_range",
        "type": "string",
        "default": None,
        "description": "crop values of latitude array - format e.g. `30. 50.`"
        },
        {
        "name": "long_range",
        "type": "string",
        "default": None,
        "description": "crop values of longitude array - format e.g. `30. 50.`"
        },
        {
        "name": "is_interp",
        "type": "string",
        "default": None,
        "description": "Should interpolation be done on input_color - valid values: None, `spatial`, `temporal`"
        },
        {
        "name": "cube_size",
        "type": "integer",
        "default": 20,
        "description": "Number of pixels (nxn) to be averaged to one value in mesh/color array."
        },
        {
        "name": "cmap",
        "type": "string",
        "default": "PiYG",
        "description": "color map (i.e. matplotlib naming - use _r seffix for the revered)"
        },
        {
        "name": "input_events",
        "type": "file",
        "default": None,
        "description": "csv with events to display on the map - columns: `first_date` (e.g. 20001231), `last_date`, `longitude`, `latitude`, `text`"
        },
        {
        "name": "title",
        "type": "string",
        "default": None,
        "description": "Title of the video"
        },
        {
        "name": "bg_padding",
        "type": "integer",
        "default": 300,
        "description": "number of pixels to pad the background world map"
        },
        {
        "name": "cube_height_multiplier",
        "type": "float",
        "default": 1,
        "description": "Change the height of the mesh cubes - 0-1 values for shorter cubes, heigher than 1 for bigger."
        },
        {
        "name": "frame_rate",
        "type": "integer",
        "default": 30,
        "description": "Frame rate per second"
        },
        {
        "name": "logo_path",
        "type": "file",
        "default": None,
        "description": "Path to institute logo"
        },],
    run=run,
    dependencies={'environment_file': env_file}
)
