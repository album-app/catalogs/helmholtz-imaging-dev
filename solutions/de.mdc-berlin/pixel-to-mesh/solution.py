from album.runner.api import setup


def create_mesh_and_save(data, file_name, level, step_size):
    import numpy as np
    from skimage import measure
    from stl import mesh
    verts, faces, normals, values = measure.marching_cubes(data, level=level, step_size=step_size)
    shape = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(faces):
        for j in range(3):
            shape.vectors[i][j] = verts[f[j], :]
    if file_name:
        shape.save(file_name)
    return shape


def run():
    from album.runner.api import get_args
    from skimage import io, filters

    input = str(get_args().input)
    radius = float(get_args().gauss_sigma)
    mesh_step_size = int(get_args().mesh_step_size)
    gauss_threshold = None
    if get_args().gauss_threshold:
        gauss_threshold = float(get_args().gauss_threshold)
    mesh_threshold = None
    if get_args().mesh_threshold:
        mesh_threshold = float(get_args().mesh_threshold)
    output_path = None
    if get_args().output:
        output_path = str(get_args().output)

    input_data = io.imread(input)
    if radius > 0:
        if gauss_threshold:
            input_data = input_data >= gauss_threshold
        input_data = filters.gaussian(input_data, sigma=radius)
    if output_path:
        if not output_path.lower().endswith('.stl'):
            output_path = output_path + '.stl'
        print("Exporting mesh to %s.." % output_path)

    shape = create_mesh_and_save(input_data, output_path, mesh_threshold, mesh_step_size)
    if not output_path:
        _display_mesh(shape)


def _display_mesh(shape):
    import vtkplotlib as vpl
    figure = vpl.figure()
    figure.render_size = (800, 600)
    vpl.mesh_plot(shape)
    vpl.show()


def pre_test():
    import numpy as np
    from skimage.draw import bezier_curve
    import tifffile as tif
    from album.runner.api import get_data_path, get_cache_path
    get_data_path().mkdir(exist_ok=True, parents=True)
    get_cache_path().mkdir(exist_ok=True, parents=True)
    dataset = get_data_path().joinpath("input.tif")
    img = np.zeros((10, 10, 10), dtype=np.float32)
    rr, cc = bezier_curve(1, 5, 5, -2, 8, 8, 2)
    img[rr, cc] = 1
    tif.imsave(dataset, img)
    out_dir = get_cache_path()

    return {
        "--input": str(dataset.absolute()),
        "--output": str(out_dir.joinpath("out.stl"))
    }


def test():
    import os
    from pathlib import Path
    from album.runner.api import get_args
    assert get_args().input is not None
    assert Path(get_args().output).exists()
    os.remove(get_args().output)
    os.remove(get_args().input)


env_file = """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.9
  - numpy=1.21.2
  - scikit-image=0.18.3
  - numpy-stl=2.16.3
  - pip=22.1.2
  - tifffile
  - pip:
    - vtkplotlib==1.4.1
"""

setup(
    group="de.mdc-berlin",
    name="pixel-to-mesh",
    version="0.1.1-SNAPSHOT",
    album_api_version="0.5.1",
    title="3D image mesh generator",
    description="Scikit based mesh generation from pixel data, either saved as STL file or displayed via vtkplotlib.",
    solution_creators=['Deborah Schmidt'],
    run=run,
    args=[{
            "name": "input",
            "type": "file",
            "description": "The input image (TIF, 3D, single channel).",
            "required": True
        }, {
            "name": "gauss_sigma",
            "type": "float",
            "description": "The sigma of the gaussian blur applied before generating the mesh (optional).",
            "required": False,
            "default": 0
        }, {
            "name": "gauss_threshold",
            "type": "float",
            "description": "Threshold used before applying the gauss filter. If not provided, no threshold is performed (optional).",
            "required": False
        }, {
            "name": "mesh_threshold",
            "type": "float",
            "description": "There value where background and foreground divide when the mesh is generated. If not provided, the average of min and max of the dataset is used (optional).",
            "required": False
        }, {
            "name": "mesh_step_size",
            "type": "integer",
            "description": "The voxel step size when generating a mesh from the volume (optional).",
            "default": 1
        }, {
            "name": "output",
            "type": "file",
            "description": "The location where the output STL file is supposed to be stored at (for example /home/user/output.stl). In case this argument is not provided, the mesh will only be displayed.",
            "required": False
    }],
    covers=[{
        "description": "Screenshot of a raw image next to the mesh generated from it and displayed using vtlplotlib.",
        "source": "cover.png"
    }],
    pre_test=pre_test,
    test=test,
    dependencies={'environment_file': env_file}
)
