from album.runner.api import setup
path_download_macos = "https://download.blender.org/release/Blender3.3/blender-3.3.1-macos-x64.dmg"
path_download_linux = "https://download.blender.org/release/Blender3.3/blender-3.3.1-linux-x64.tar.xz"
path_download_windows = "https://download.blender.org/release/Blender3.3/blender-3.3.1-windows-x64.zip"
download_name_linux = "blender-3.3.1-linux-x64.tar.xz"
download_name_windows = "blender-3.3.1-windows-x64.zip"
download_name_macos = "blender-3.3.1-macos-x64.dmg"
path_run_linux = "blender-3.3.1-linux-x64/blender"
path_run_windows = "blender-3.3.1-windows-x64\\blender.exe"
path_run_macos = "Contents/MacOS/Blender"


def install():
    from album.runner.api import get_cache_path
    import sys
    get_cache_path().mkdir(exist_ok=True, parents=True)
    operation_system = sys.platform
    if operation_system == "linux":
        __install_linux()
    elif operation_system == "darwin":
        __install_macos()
    else:
        __install_windows()
    # list_files(str(get_app_path()))


def __install_linux():
    from album.runner.api import get_cache_path, extract_tar, get_app_path
    download_target = get_cache_path().joinpath(download_name_linux)
    download(path_download_linux, download_target)
    extract_tar(download_target, get_app_path())


def __install_macos():
    import dmglib
    from distutils.dir_util import copy_tree
    import os
    from album.runner.api import get_cache_path, get_app_path
    get_app_path().mkdir(exist_ok=True, parents=True)
    download_target = get_cache_path().joinpath(download_name_macos)
    download(path_download_macos, download_target)
    dmg = dmglib.DiskImage(download_target)

    if dmg.has_license_agreement():
        print('Cannot attach disk image.')
        return

    for mount_point in dmg.attach():
        for entry in os.listdir(mount_point):
            print('{} -- {}'.format(mount_point, entry))
        copy_tree(mount_point + os.sep + 'Blender.app', str(get_app_path()))

    dmg.detach()


def __install_windows():
    from album.runner.api import get_cache_path, get_app_path
    download_target = get_cache_path().joinpath(download_name_windows)
    download(path_download_windows, download_target)
    extract_zip(download_target, get_app_path())


def extract_zip(in_zip, out_dir):
    from pathlib import Path
    import zipfile
    from album.runner.album_logging import get_active_logger
    out_path = Path(out_dir)

    if not out_path.exists():
        out_path.mkdir(parents=True)

    get_active_logger().info(f"Extracting {in_zip} to {out_dir}...")

    with zipfile.ZipFile(in_zip, 'r') as zip_ref:
        zip_ref.extractall(out_dir)


def _get_session():
    import requests
    from requests.adapters import HTTPAdapter
    from urllib3 import Retry
    s = requests.Session()
    retry = Retry(connect=3, backoff_factor=0.5)

    adapter = HTTPAdapter(max_retries=retry)

    s.mount("http://", adapter)
    s.mount("https://", adapter)

    return s


def _request_get(url):
    """Get a response from a request to a resource url."""
    from album.ci.utils.zenodo_api import ResponseStatus
    with _get_session() as s:
        r = s.get(url, allow_redirects=True, stream=True)

        if r.status_code != ResponseStatus.OK.value:
            raise ConnectionError("Could not connect to resource %s!" % url)

        return r


def download(str_input, output):
    with _get_session() as s:
        r = s.get(str_input, allow_redirects=True, stream=True)
        with open(output, "wb") as out:
            out.write(r.content)


def pre_test():
    import tempfile
    from pathlib import Path
    from album.runner.api import get_package_path, get_cache_path
    dataset = Path(get_package_path()).joinpath("test", "project.blend")
    out_dir = Path(tempfile.TemporaryDirectory(dir=get_cache_path()).name)
    out_dir.mkdir()
    return {
        "--input": str(dataset.absolute()),
        "--output_rendering": str(out_dir.joinpath("out.png")),
        "--output_blend": str(out_dir.joinpath("out.blend")),
        "--resolution_percentage": "20",
    }


def test():
    from pathlib import Path
    from album.runner.api import get_args
    import shutil
    assert get_args().input is not None
    assert Path(get_args().output_rendering).exists()
    assert Path(get_args().output_blend).exists()
    shutil.rmtree(Path(get_args().output_blend).parent)


def _get_blender_executable():

    import sys
    operation_system = sys.platform
    if operation_system == "linux":
        return path_run_linux
    elif operation_system == "darwin":
        return path_run_macos
    else:
        return path_run_windows


def list_files(startpath):
    import os
    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        print('{}{}/'.format(indent, os.path.basename(root)))
        subindent = ' ' * 4 * (level + 1)
        for f in files:
            print('{}{}'.format(subindent, f))


def run_blender_script(script, *params):
    import subprocess
    from album.runner.api import get_app_path, get_args
    blender_path = str(get_app_path().joinpath(_get_blender_executable()))
    if get_args().output_rendering:
        args = [blender_path, "-b", "-d", "-noaudio", "--debug-gpu-force-workarounds", "-P", script, "--"]
    else:
        args = [blender_path, "-d", "-noaudio", "--debug-gpu-force-workarounds", "-P", script, "--"]
    args.extend(params)
    subprocess.run(args)


def run():
    from pathlib import Path
    from album.runner.api import get_args, get_package_path
    resolution_percentage = get_args().resolution_percentage
    input_dir = get_args().input
    output_rendering = get_args().output_rendering
    output_blend = Path(get_args().output_blend).absolute()
    run_blender_script(str(Path(get_package_path()).joinpath("blender_script.py").absolute()),
                       str(resolution_percentage), str(input_dir), str(output_blend), str(output_rendering))


setup(
    group="de.mdc-berlin",
    name="blender-style-glass",
    version="0.1.0-SNAPSHOT",
    album_api_version="0.5.1",
    solution_creators=['Deborah Schmidt'],
    cite=[{
        "text": "Blender Online Community: Blender - a 3D modelling and rendering package (2018). Stichting Blender Foundation, Amsterdam.",
        "url": "http://www.blender.org"
    }],
    title="Blender styling meshes with glass material",
    description="This solution applies glass material to all meshes in a Blender file and renders it.",
    covers=[{
       "description": "This rendering was generated based on data from the following publication: Andreas Müller, Deborah Schmidt, C. Shan Xu, Song Pang, Joyson Verner D’Costa, Susanne Kretschmar, Carla Münster, Thomas Kurth, Florian Jug, Martin Weigert, Harald F. Hess, Michele Solimena; 3D FIB-SEM reconstruction of microtubule–organelle interaction in whole primary mouse β cells. J Cell Biol 1 February 2021; 220 (2): e202010039. doi: https://doi.org/10.1083/jcb.202010039",
        "source": "cover.jpg"
    }],
    install=install,
    pre_test=pre_test,
    test=test,
    run=run,
    args=[{
        "name": "input",
        "type": "file",
        "description": "Path to the .blend input file.",
        "required": True
    }, {
        "name": "output_rendering",
        "type": "file",
        "description": "Path for storing the rendering.",
        "required": False
    }, {
        "name": "output_blend",
        "type": "file",
        "description": "Path for storing the Blender project for rendering.",
        "required": False
    }, {
        "name": "resolution_percentage",
        "default": 100,
        "type": "integer",
        "description": "Resolution of rendering (integer value from 1 - 100, optional)."
    }],
    dependencies={'parent': {'resolve_solution': 'de.mdc-berlin:launch-blender'}}
)
