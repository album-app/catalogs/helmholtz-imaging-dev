import net.imagej.ImageJ;
import org.scijava.Context;
import sc.fiji.labkit.ui.LabkitFrame;
import sc.fiji.labkit.ui.inputimage.SpimDataInputImage;

public class Main {
	public static void main(String...args) {
		String input = args[0];
        Context context = new ImageJ().context();
        if(input.endsWith(".xml")) {
	        SpimDataInputImage img = new SpimDataInputImage(input, 0);
            LabkitFrame.showForImage(context, img);
        } else {
            LabkitFrame.showForFile(context, input);
        }
	}
}
