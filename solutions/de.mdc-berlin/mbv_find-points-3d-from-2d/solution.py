from album.runner.api import get_args, setup


def run():
    from find_points_3d_from_2d import find_points

    args = get_args()
    find_points(args.image_filename, args.csv_filename, args.output_filename, args.sigma, args.debug)


setup(
    group="de.mdc-berlin",
    name="mbv_find-points-3d-from-2d",
    version="0.1.0-SNAPSHOT",
    title=
    "macrophage-brain-vasculature find 3D points from 2D annotations solution",
    description=
    "A solution for finding 3D coordinates from 2D annotations with respect to a target image",
    solution_creators=["Kyle Harrington", "Jan Philipp Albrecht"],
    tags=["vasculature", "segmentation", "projection"],
    license="Apache v2",
    documentation=["README.md"],
    covers=[{
        "description": "Annotation of Macrophage signal intensities",
        "source": "find_points_3d_from_2d.png",
    }],
    album_api_version="0.5.1",
    args=[
        {
            "name": "csv_filename",
            "description": "csv filename"
        },
        {
            "name": "image_filename",
            "description": "image filename"
        },
        {
            "name": "output_filename",
            "description": "output csv filename"
        },
        {
            "name": "sigma",
            "type": "float",
            "description": "Gaussion blur sigma parameter. Usually >2."
        },
        {
            "name": "debug",
            "default": False,
            "description": "debug mode (default: False)",
        }
    ],
    run=run,
    dependencies={
        'parent': {
            'group': 'de.mdc-berlin',
            'name': 'mbv_parent',
            'version': '0.1.0-SNAPSHOT'
        }
    }
)
