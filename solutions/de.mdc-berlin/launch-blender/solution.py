from album.runner.api import setup

env_file = """channels:
  - defaults
dependencies:
  - python=3.9
  - pip
  - requests
  - pip:
    - dmglib
"""
path_download_macos = "https://download.blender.org/release/Blender3.3/blender-3.3.1-macos-x64.dmg"
path_download_linux = "https://download.blender.org/release/Blender3.3/blender-3.3.1-linux-x64.tar.xz"
path_download_windows = "https://download.blender.org/release/Blender3.3/blender-3.3.1-windows-x64.zip"
download_name_linux = "blender-3.3.1-linux-x64.tar.xz"
download_name_windows = "blender-3.3.1-windows-x64.zip"
download_name_macos = "blender-3.3.1-macos-x64.dmg"
path_run_linux = "blender-3.3.1-linux-x64/blender"
path_run_windows = "blender-3.3.1-windows-x64\\blender.exe"
path_run_macos = "blender-3.3.1-macos-x64/blender"


def install():
    from album.runner.api import get_cache_path
    import sys
    get_cache_path().mkdir(exist_ok=True, parents=True)
    operation_system = sys.platform
    if operation_system == "linux":
        __install_linux()
    elif operation_system == "darwin":
        __install_macos()
    else:
        __install_windows()


def __install_linux():
    from album.runner.api import get_cache_path, extract_tar, get_app_path
    download_target = get_cache_path().joinpath(download_name_linux)
    download(path_download_linux, download_target)
    extract_tar(download_target, get_app_path())


def __install_macos():
    import dmglib
    import os
    from album.runner.api import get_cache_path
    download_target = get_cache_path().joinpath(download_name_macos)
    download(path_download_macos, download_target)
    dmg = dmglib.DiskImage(download_target)

    if dmg.has_license_agreement():
        print("Cannot attach disk image.")
        return

    for mount_point in dmg.attach():
        for entry in os.listdir(mount_point):
            print('{} -- {}'.format(mount_point, entry))

    dmg.detach()


def __install_windows():
    from album.runner.api import get_cache_path, get_app_path
    download_target = get_cache_path().joinpath(download_name_windows)
    download(path_download_windows, download_target)
    extract_zip(download_target, get_app_path())


def extract_zip(in_zip, out_dir):
    from pathlib import Path
    import zipfile
    from album.runner.album_logging import get_active_logger
    out_path = Path(out_dir)

    if not out_path.exists():
        out_path.mkdir(parents=True)

    get_active_logger().info(f"Extracting {in_zip} to {out_dir}...")

    with zipfile.ZipFile(in_zip, 'r') as zip_ref:
        zip_ref.extractall(out_dir)


def _get_session():
    import requests
    from requests.adapters import HTTPAdapter
    from urllib3 import Retry
    s = requests.Session()
    retry = Retry(connect=3, backoff_factor=0.5)

    adapter = HTTPAdapter(max_retries=retry)

    s.mount("http://", adapter)
    s.mount("https://", adapter)

    return s


def _request_get(url):
    """Get a response from a request to a resource url."""
    from album.ci.utils.zenodo_api import ResponseStatus
    with _get_session() as s:
        r = s.get(url, allow_redirects=True, stream=True)

        if r.status_code != ResponseStatus.OK.value:
            raise ConnectionError("Could not connect to resource %s!" % url)

        return r


def download(str_input, output):
    with _get_session() as s:
        r = s.get(str_input, allow_redirects=True, stream=True)
        with open(output, "wb") as out:
            out.write(r.content)


def run_blender_script(input):
    import subprocess
    from album.runner.api import get_app_path, get_args
    blender_path = f"{get_app_path()}/{path_run_linux}"
    args = [blender_path]
    if get_args().input:
        args.append(input)
    subprocess.run(args)


def run():
    from pathlib import Path
    from album.runner.api import get_args
    input_blend = get_args().input
    if input_blend:
        input_blend = str(Path(input_blend).absolute())
    run_blender_script(input_blend)


setup(
    group="de.mdc-berlin",
    name="launch-blender",
    version="0.1.0-SNAPSHOT",
    album_api_version="0.5.1",
    solution_creators=['Deborah Schmidt'],
    cite=[{
        "text": "Blender Online Community: Blender - a 3D modelling and rendering package (2018). Stichting Blender Foundation, Amsterdam.",
        "url": "http://www.blender.org"
    }],
    title="Launch Blender 2.83.13",
    description="This solution launches Blender, optionally with a specific .blend file as input.",
    covers=[{
       "description": "",
        "source": "cover.png"
    }],
    install=install,
    run=run,
    args=[{
        "name": "input",
        "type": "file",
        "description": "Path to input .blend file",
        "required": False
    }],
    dependencies={'environment_file': env_file}
)
