This solution can be used on 2D data. 

## Exemplary application
### Input data
Here are two example inputs shipped with this solution:
- ![The raw input data](./sample/input_raw.tif), a fictional raw image displaying cell like objects
- ![The segmentation input data](./sample/input_mask.tif), a mask which was acquired for the raw input image.

### Solution call
The solution was called like this:
```
album run cell-orientation --input_raw ./sample/input_raw.tif --input_mask ./sample/input_mask.tif --output ./sample/output 
```

The solution executes the following steps:
1. It splits the segmentation into individual labels for each object.
2. It calculates region properties for each label.
3. Optionally, it sorts out labels which are too small or too big (see solution arguments).
4. It skeletonizes the remaining labels.
5. For each label, it calculates the distance map of the label to the background.
6. The point which has the highest distance to the background in all directions is chosen as the center of the cell.
7. The distance of this point to the background is chosen as the radius of the cell, all pixels inside this radius are considered part of the cell, all other pixels are considered extensions of the cell.
8. The directional vector of the cell extensions is calculated by calculating the mean of the distance between all pixels on the skeleton of the extensions and the center of the cell. 


### Results
The resulting plots were generated and saved into the provided output directory `./sample/output`.

#### Directional vectors
The first plot displays the directional vector of each cell as an arrow:
![output plot 1](./sample/output/directions_0:1397:0:801.png)

#### Mean directional vectors of tiles
The other plots are displaying the mean of multiple cells in tiles - the set of tile sizes can be adjusted via the solution arguments.

The color of a tile is determined by the orientation of the mean of the directional vectors of all cells in the same tile.

The opacity of a tile depends on the number of cells in a tile and the length of the mean of all directional vectors in this tile. It can be described by this formula, where `max_count` depends on the size of the tile and `max_length` is a hardcoded value: 
```
cell_count_score = tile_cell_count / max_count
vector_length_score = tile_mean_vector_length / max_length
alpha = cell_count_score * vector_length_score
```
Imagine `cell_count_score` being high if many cells (relatively to the tile size) contributed to the mean directional vector and `vector_length_score` being high if cells in the same tile have similar directions.

A tile is more opaque (the directional color will be displayed stronger) if the tile includes many cells with similar orientation of their extensions. 

![output plot 2](./sample/output/directions_0:1397:0:801_tile100.png)
![output plot 3](./sample/output/directions_0:1397:0:801_tile250.png)
![output plot 4](./sample/output/directions_0:1397:0:801_tile500.png)